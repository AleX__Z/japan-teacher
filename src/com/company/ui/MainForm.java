package com.company.ui;

import com.company.lang.WordDao;

import javax.swing.*;
import java.awt.*;
import java.io.File;

public class MainForm extends JFrame {
	private JButton testButton;
	private JButton dictionaryButton;
	private JPanel root;
	private JButton test2Button;
	private Test1Form test1Form;
	private Test2Form test2Form;
	private DictionaryForm dictionaryForm;

	public MainForm() throws HeadlessException {
		super("Japan Teacher");
		dictionaryButton.addActionListener(e -> {
			if (dictionaryForm == null || !dictionaryForm.isVisible())
				dictionaryForm = new DictionaryForm();
			else
				dictionaryForm.setVisible(true);
		});
		testButton.addActionListener(e -> {
			if (test1Form == null || !test1Form.isVisible())
				test1Form = new Test1Form();
			else
				test1Form.setVisible(true);
		});
		test2Button.addActionListener(e -> {
			if (test2Form == null || !test2Form.isVisible())
				test2Form = new Test2Form();
			else
				test2Form.setVisible(true);
		});

		setContentPane(root);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		pack();
		setSize(300, 150);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
		setVisible(true);
	}

	public static void main(String[] args) {
		if (new File(WordDao.FILE_DICTIONARY_NAME).exists())
			new MainForm();
		else
			JOptionPane.showMessageDialog(
					null,
					"File " + WordDao.FILE_DICTIONARY_NAME + " no found !",
					"Error",
					JOptionPane.ERROR_MESSAGE
			);
	}
}
